#[macro_use]
extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;

mod models;
mod schema;
mod lib;

use crate::models::{Bays,NewBays};
use std::env;

/* ------------------------------------ */
/* | Establish a connection to the    | */
/* | postgresql database via wrapper  | */
/* | to lib.rs.                       | */
/* |                                  | */
/* | Takes in nothing.                | */
/* |                                  | */
/* | Returns a connection object.     | */
/* ------------------------------------ */
fn establish_connection() -> PgConnection {
    lib::establish_connection()
}

/* ------------------------------------ */
/* | Create entry into the postgresql | */
/* | database.                        | */
/* |                                  | */
/* | Takes in a connection, bay, size,| */
/* | serial_number, and hostname.     | */
/* |                                  | */
/* | Returns a confirmation object.   | */
/* ------------------------------------ */
fn create_entry<'a>(conn: &PgConnection, bay: &'a str, size: &'a str, serial_number: &'a str, hostname: &'a str) -> Bays {
    use schema::bays;
    let new_entry = NewBays {
        bay: bay,
        size: size,
        serial_number: serial_number,
        hostname: hostname,
    };
    diesel::insert_into(bays::table)
        .values(&new_entry)
        .get_result(conn)
        .expect("Error writing new entry")
}

/* ------------------------------------ */
/* | Main function                    | */
/* ------------------------------------ */
fn main() {
    let csv_inputs = lib::read_csv_inputs();
    let device_files = lib::get_device_files();
    let connection = establish_connection();
    let manual: bool;
    match env::var("MANUAL") {
        Ok(_v) => manual = _v.parse().unwrap(),
        Err(_e) => manual = false,
    };
    let hostname = lib::get_local_hostname(manual).unwrap();
    if manual {
        for input in &csv_inputs {
            create_entry(&connection, &input.bay, &input.size, &input.serial_number, &hostname);
        }

    } else {
        for device in device_files {
            let disk_info = lib::aggregate_device_info(device.unwrap().display());
            for input in &csv_inputs {
                if disk_info.size == input.size && disk_info.serial_number == input.serial_number {
                    create_entry(&connection, &input.bay, &disk_info.size, &disk_info.serial_number, &hostname);
                }
            }
        }
    }
}
