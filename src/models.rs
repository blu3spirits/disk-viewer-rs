use super::schema::bays;
#[derive(Queryable)]
pub struct Bays {
    pub id: i32,
    pub bay: String,
    pub serial_number: String,
    pub size: String,
    pub hostname: String,
}


#[derive(Insertable)]
#[table_name="bays"]
pub struct NewBays<'a> {
    pub bay: &'a str,
    pub size: &'a str,
    pub serial_number: &'a str,
    pub hostname: &'a str,
}
