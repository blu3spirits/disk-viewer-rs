table! {
    bays (id) {
        id -> Int4,
        bay -> Text,
        serial_number -> Text,
        size -> Text,
        hostname -> Text,
    }
}
