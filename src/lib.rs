use diesel::prelude::*;
use dotenv::dotenv;
use std::env;
use std::path::Display;
use glob::glob;
use std::path::PathBuf;
use std::fs;
use hostname;

#[derive(Debug)]
pub struct ScannedDisk {
    pub size: String,
    pub serial_number: String,
}

#[derive(Debug)]
pub struct Disk {
    pub bay: String,
    pub size: String,
    pub serial_number: String,
}



/* ------------------------------------ */
/* | Establish a connection to the    | */
/* | postgreqsql database.            | */
/* |                                  | */
/* | Takes in nothing.                | */
/* |                                  | */
/* | Returns a connection object.     | */
/* ------------------------------------ */
pub fn establish_connection() -> PgConnection {
    dotenv().ok();

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&db_url).unwrap_or_else(|_| panic!("Error connecting to {}", db_url))
}

/* ------------------------------------ */
/* | Get device information files.    | */
/* | These are located at:            | */
/* | /sys/class/block/sdX             | */
/* |                                  | */
/* | Takes in nothing.                | */
/* |                                  | */
/* | Returns a vector of Paths.       | */
/* ------------------------------------ */
pub fn get_device_files() -> Vec<Result<PathBuf, glob::GlobError>> {
    let mut ret = Vec::new();
    for e in glob("/sys/class/block/sd?").unwrap() {
        ret.push(e);
    }
    return ret;
}

/* ------------------------------------ */
/* | Get the device size of a given   | */
/* | device information file.         | */
/* |                                  | */
/* | Takes in a target path as a      | */
/* | string.                          | */
/* |                                  | */
/* | Returns a signed integer.        | */
/* ------------------------------------ */
fn get_device_size(target: String) -> i64 {
    let data = fs::read_to_string(target)
                    .expect("Unable to read file");
    let raw_device_size: i64 = data.trim().parse().unwrap();
    let device_size = (raw_device_size * 512) / 1_000_000_000;
    return device_size;
}

/* ------------------------------------ */
/* | Get the serial number of a device| */
/* | of a given device information    | */
/* | file.                            | */
/* |                                  | */
/* | Takes in a target path as a      | */
/* | string.                          | */
/* |                                  | */
/* | Returns a serial number as       | */
/* | string.                          | */
/* ------------------------------------ */
fn get_serial_number(target: String) -> String {
    let data = fs::read_to_string(target)
                    .expect("Unable to read file");
    let split = data.split(" ");
    let vec: Vec<&str> = split.collect();
    let mut disk_vector = Vec::new();
    for x in vec {
        if x != "" && x != "\n" {
            if x.ends_with("\n") {
                disk_vector.push(&x[..x.len()-1]);
            } else {
                disk_vector.push(x);
            }
        }
    }
    let serial_number = disk_vector[disk_vector.len()-1];
    return serial_number.to_string();
}

/* ------------------------------------ */
/* | Concatenation method for         | */
/* | combining the results from       | */
/* | get_serial_number() and          | */
/* | get_device_size() into one struct| */
/* |                                  | */
/* | Takes in a device(sdX) and       | */
/* | formats it into a usable target  | */
/* |                                  | */
/* | Returns a struct containing the  | */
/* | gathered disk information        | */
/* ------------------------------------ */
pub fn aggregate_device_info(device: Display) -> ScannedDisk {
    let serial_number_target = format!("{}{}", device, "/device/wwid");
    let serial_number = get_serial_number(serial_number_target);
    let size_target = format!("{}{}", device, "/size");
    let size = get_device_size(size_target);
    let disk_info = ScannedDisk {
        size: size.to_string()+"GB",
        serial_number: serial_number,
    };
    return disk_info;
}

/* ------------------------------------ */
/* | Reads the CSV file for sysadmin  | */
/* | input regarding disk location,   | */
/* | size, and serial #. Then compares| */
/* | with already obtained information| */
/* | from the system.                 | */
/* |                                  | */
/* | Takes in nothing.                | */
/* |                                  | */
/* | Returns a vector of structs      | */
/* | containing information about the | */
/* | disk gathered from the CSV file  | */
/* ------------------------------------ */
pub fn read_csv_inputs() -> Vec<Disk> {
    let target = "./src/disks.csv";
    let data = fs::read_to_string(target)
                .expect("Unable to read CSV file");
    let mut reader = csv::Reader::from_reader(data.as_bytes());
    let mut inputs: Vec<Disk> = Vec::new();
    for record in reader.records() {
        let record = record.unwrap();
        let disk_input = Disk {
            bay: record[0].to_string(),
            serial_number: record[1].to_string(),
            size: record[2].to_string(),
        };
        inputs.push(disk_input);
    }
    return inputs;
}

/* ------------------------------------ */
/* | Uses the hostname crate to give  | */
/* | hostname information             | */
/* |                                  | */
/* | Takes in nothing                 | */
/* |                                  | */
/* | Returns an Option(String)        | */
/* ------------------------------------ */
pub fn get_local_hostname(manual: bool) -> Option<String> {
    if manual {
        match env::var("HOSTNAME") {
            Ok(v) => return Some(v),
            Err(_e) => return Some("default".to_string()),
        }
    } else {
        return hostname::get_hostname();
    }
}


