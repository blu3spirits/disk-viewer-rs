# Disk Viewer
## Install
- Install [rust] (https://rustup.rs/)
- clone this repo
  `https://gitlab.com/blu3spirits/disk-viewer-rs.git`
- cargo build

## Install binary
- Download the [binary] (https://gitlab.com/blu3spirits/disk-viewer-rs/releases)
- Extract the tarball
  `tar xf <tarball>`

## Running
- Populate the CSV with your disk data. There is some example data preloaded

Running with automatic scanning (cargo)
```
DATABASE_URL=postgres://route/to/database/table cargo run
```

Running with automatic scanning (binary)
```
DATABASE_URL=postgres://route/to/database/table ./disk-viewer
```

Running with manual inputs (cargo)
```
MANUAL=true HOSTNAME=<hostname> DATABASE_URL=postgres://route/to/database/table cargo run
```

Running with manual inputs (binary)
```
MANUAL=true HOSTNAME=<hostname> DATABASE_URL=postgres://route/to/database/table ./disk-viewer
```
